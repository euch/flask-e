import os
from flask import Flask

from flask.sessions import SessionInterface
from beaker.middleware import SessionMiddleware

from .database import db


session_opts = {
    'session.type': 'ext:memcached',
    'session.url': os.environ['MEMCACHED_ADDRESS'],
    'session.data_dir': './flask-e',
}


class BeakerSessionInterface(SessionInterface):
    def open_session(self, app, request):
        session = request.environ['beaker.session']
        return session

    def save_session(self, app, session, response):
        session.save()


def create_app():
    app = Flask(__name__)
    app.config.from_object(os.environ['APP_SETTINGS'])

    db.init_app(app)
    with app.test_request_context():
        db.create_all()

    if app.debug == True:
        try:
            from flask_debugtoolbar import DebugToolbarExtension
            toolbar = DebugToolbarExtension(app)
        except:
            pass

    import app.news.controllers as news
    import app.monitoring.controllers as monitoring
    import app.general.controllers as general

    app.register_blueprint(general.module)
    app.register_blueprint(news.module)
    app.register_blueprint(monitoring.module)

    app.wsgi_app = SessionMiddleware(app.wsgi_app, session_opts)
    app.session_interface = BeakerSessionInterface()

    return app
