from sqlalchemy import Column, Integer, ForeignKey
from sqlalchemy.orm import relationship

from app.database import db

def dump_datetime(value):
    """Deserialize datetime object into string form for JSON processing."""
    if value is None:
        return None
    return [value.strftime("%Y-%m-%d"), value.strftime("%H:%M:%S")]

class News(db.Model):
    __tablename__ = 'news'

    id = db.Column(db.Integer, primary_key=True)
    link = db.Column(db.String(5000), nullable=False)
    save_date = db.Column(db.DateTime(timezone=True), nullable=False)
    pub_date = db.Column(db.DateTime(timezone=True), nullable=False)
    title = db.Column(db.String(5000), nullable=False)
    content = db.Column(db.Text)
    news_source_parent_fk = Column(Integer, ForeignKey('news_source.id'), nullable=False)
    source = relationship("NewsSource")

    def __repr__(self):
        return self.name

    @property
    def serialize(self):
        return {
            'id': self.id,
            'link': self.link,
            'saved': dump_datetime(self.save_date),
            'published': dump_datetime(self.pub_date),
            'title': self.title,
            'content': self.content,
            'source': self.source.serialize
        }


class NewsSource(db.Model):
    __tablename__ = "news_source"

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(5000), nullable=False)
    url = db.Column(db.String(5000), nullable=False)

    @property
    def serialize(self):
        return {
            'id': self.id,
            'name': self.name,
            'link': self.url
        }
