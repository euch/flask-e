from flask import (
    Blueprint,
    render_template,
    flash,
    abort,
    request,
    current_app,
    session,
    make_response,
    redirect,
    jsonify
)
from json import (
    dumps,
    loads
)
from sqlalchemy.exc import SQLAlchemyError

from .models import News

module = Blueprint('news', __name__)


def log_error(*args, **kwargs):
    current_app.logger.error(*args, **kwargs)


NEWS_PER_PAGE = 'news-per-page'
DEFAULT_NEWS_PER_PAGE = 10
RANGE_NEWS_PER_PAGE = list(range(1, 101))

NEWS_FILTER = 'news-filter'
DEFAULT_FILTER = ''
MIN_FILTER = 3

PAGE = "page"


@module.route('/news/', methods=['GET'])
@module.route('/news/page/<int:page>/', methods=['GET'])
def index(page=1):
    return render_template('news/index.html', object_list=__load__(True, page), filter=__filter__(),
                           per_page=__per_page__())


@module.route('/news/csv_dump', methods=['GET'])
def csv_dump():
    sp = '\t'

    def row(news):
        return news.pub_date.strftime("%d.%m.%Y") + sp + news.source.name.replace('\n', ' ') + sp + news.title.replace('\n', ' ')

    title = 'publication date' + sp + 'source' + sp + 'title' + '\n'
    txt = title + '\n'.join(map(row, __load__(False)))
    response = make_response(txt)
    response.headers['Content-Disposition'] = "attachment; filename=news.csv"
    return response


@module.route('/news/json_dump', methods=['GET'])
def json_dump():
    def f(n):
        return n.serialize
    jnews = dumps(__load__(False), default=f)
    response = make_response(jnews)
    response.headers['Content-Disposition'] = "attachment; filename=news.json"
    return response


@module.route('/news/set_filter', methods=['POST'])
def set_filter():
    f = request.form[NEWS_FILTER]
    session[NEWS_FILTER] = f if len(f) > MIN_FILTER else DEFAULT_FILTER
    return redirect("/news/", code=302)



@module.route('/news/set_per_page', methods=['POST'])
def set_per_page():
    try:
        f = int(request.form[NEWS_PER_PAGE])
        if f in RANGE_NEWS_PER_PAGE:
            session[NEWS_PER_PAGE] = f
    except ValueError:
        log_error('non-integer sent to /news/set_per_page')
    return redirect("/news/", code=302)


def __filter__():
    return session[NEWS_FILTER] if session.has_key(NEWS_FILTER) else DEFAULT_FILTER


def __per_page__():
    return session[NEWS_PER_PAGE] if session.has_key(NEWS_PER_PAGE) else DEFAULT_NEWS_PER_PAGE


def __load__(paginate, page=0):
    news = None

    def paginated_filter():
        return News.query.filter(News.title.contains(session[NEWS_FILTER])) \
            .order_by(News.pub_date.desc()).paginate(page, per_page=__per_page__(), error_out=True)

    def paginated_nofilter():
        return News.query.order_by(News.pub_date.desc()).paginate(page, per_page=__per_page__(), error_out=True)

    def nopanated_filter():
        return News.query.filter(News.title.contains(session[NEWS_FILTER])).order_by(News.pub_date).all()

    try:
        if not paginate and session.has_key(NEWS_FILTER):
            news = nopanated_filter()
        else:
            news = paginated_filter() if session.has_key(NEWS_FILTER) else paginated_nofilter()
    except SQLAlchemyError as e:
        log_error('Error while querying database', exc_info=e)
        flash('There was uncaught database query', 'danger')
        abort(500)
    return news
