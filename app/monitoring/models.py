from sqlalchemy import Column, Integer, ForeignKey
from sqlalchemy.orm import relationship

from app.database import db


class ServiceTarget(db.Model):
    __tablename__ = 'service_targets'

    name = db.Column(db.String(31), primary_key=True)
    host = db.Column(db.String(255), nullable=False)
    port = db.Column(db.Integer, nullable=False)

    # def __repr__(self):
    #     return self.name


class ServiceState:
    def __init__(self, name, state, comment="", info=""):
        self.name = name
        self.state = state
        self.comment = comment
        self.info = info

    # def __str__(self):
    #     return self.target.name


class OttdStatus(db.Model):
    __tablename__ = 'ottd_status'

    id = db.Column(db.Integer, primary_key=True)
    probe_date = db.Column(db.DateTime(timezone=True), nullable=False)
    online = db.Column(db.Boolean, nullable=False)
    companies_online = db.Column(db.SmallInteger, nullable=True)
    players_online = db.Column(db.SmallInteger, nullable=True)
    game_date = db.Column(db.Date, nullable=True)
    ottd_source = Column(Integer, ForeignKey('ottd_source.id'), nullable=False)
    source = relationship("OttdSource")

    # def __repr__(self):
    #     return self.source.name + self.probe_date


class OttdSource(db.Model):
    __tablename__ = "ottd_source"

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(5000), nullable=False)
    enabled = db.Column(db.Boolean, nullable=False)