from flask import (
    Blueprint,
    render_template,
    flash,
    abort,
    current_app
)

from sqlalchemy.exc import SQLAlchemyError
from app.database import db

import os
import socket

from .models import ServiceState, ServiceTarget, OttdStatus

import memcache

module = Blueprint('monitoring', __name__)

CACHE_TTL = 20

mc = memcache.Client([os.environ['MEMCACHED_ADDRESS']], debug=0)


def log_error(*args, **kwargs):
    current_app.logger.error(*args, **kwargs)


@module.route('/mon/', methods=['GET'])
def index():
    return render_template('monitoring/index.html', object_list=check_targets())


def check_targets():
    data = None
    try:
        data = __service_data__()
        data.extend(__ottd_data__())
    except SQLAlchemyError as e:
        log_error('Error while querying database', exc_info=e)
        flash('There was uncaught database query', 'danger')
        abort(500)
    return data


def __service_data__():
    return list(map(__check_service__, ServiceTarget.query.all()))


def __ottd_data__():
    heads = db.engine.execute(
        "SELECT DISTINCT ON (ottd_source) id, probe_date, ottd_source FROM   ottd_status ORDER  BY ottd_source, probe_date DESC, id;")
    headIds = []
    for head in heads:
        headIds.append(head[0])
    for id in headIds: print(id)
    return list(map(__format_ottd__, OttdStatus.query.filter(OttdStatus.id.in_(headIds)).all()))


def __format_ottd__(st):
    info = "Clients: {0} Companies: {1} Year: {2}".format(st.players_online, st.companies_online, st.game_date) if st.online else ""
    return ServiceState(name="OpenTTD", info=info, state=st.online, comment=st.source.name)


def __check_service__(service_target):
    def __check_port__():
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        result = sock.connect_ex((service_target.host, service_target.port))
        state = True if result == 0 else False
        return ServiceState(name=service_target.name, state=state)

    key = str(hash(service_target.name))
    service_state = mc.get(key)
    if not service_state:
        if isinstance(service_target, ServiceTarget):
            service_state = __check_port__()
            mc.set(key, service_state, CACHE_TTL)
    return service_state
