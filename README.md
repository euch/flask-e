# Flask news aggregation and monitoring

The web application has two tabs: 

1. monitoring: Ping check for services described in the database (it's easy operation performed by flask controller); Openttd availability log analysys (log is stored in the database and prepared by [Scala-written crawler](https://bitbucket.org/euch/crawl-e)

1. news -- displaying news feed from the database. News feed is also prepared by the same [crawler](https://bitbucket.org/euch/crawl-e)


Many thanks to Bosha. I got source code structure from his great article (it's in Russian)
[правильная структура flask приложения](https://the-bosha.ru/2016/06/03/python-flask-freimvork-pravilnaia-struktura-prilozheniia/).

## Setup

```
git clone https://github.com/bosha/flask-app-structure-example/
cd flask-app-structure-example
virtualenv -p python3 env
source env/bin/activate
pip install -r requipments/development.txt
export APP_SETTINGS="config.DevelopmentConfig"
# DBUSERNAME, DBPASSWORD и DBNAME необходимо заменить на свои реквизиты доступа к БД
export DATABASE_URL='postgresql://DBUSERNAME:DBPASSWORD@localhost/DBNAME'
export MEMCACHED_ADDRESS=127.0.0.1:11211
python manage.py db init
python manage.py db migrate
python manage.py db upgrade
python manage.py runserver
```